﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Class representing a visible area on the map.
/// </summary>
public class VisibleArea 
{
	public Transform Target
	{
		get;
		private set;
	}

	public int Radius
	{
		get;
		private set;
	}

	private Vector3 position = Vector3.zero;
	public Vector3 Position
	{
		get
		{
			if(Target != null)
			{
				return Target.position;
			}
			else
			{
				return position;
			}
		}
	}



	public VisibleArea(Transform _Target, int _Radius)
	{
		Target = _Target;
		Radius = Mathf.Max(0, _Radius);
	}

	public VisibleArea(Vector3 _Position, int _Radius)
	{
		Target = null;
		Radius = _Radius;

		position = _Position;
	}
}