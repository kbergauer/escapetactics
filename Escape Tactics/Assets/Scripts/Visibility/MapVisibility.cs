﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Class used for handling general map visibility.
/// </summary>
public class MapVisibility 
{
	static private MapVisibility instance;



	private HexMap map;
	private List<VisibleArea> areas = null;



	private MapVisibility(HexMap _Map)
	{
		map = _Map;
		areas = new List<VisibleArea>();
	}

	private void Destroy()
	{
		map = null;

		areas.Clear();
		areas = null;
	}

	private void UpdateVisibleAreas()
	{
		// Temporarily save visibilities to avoid updating tiles multiple times
		Dictionary<string, HexTile.EVisibility> tileVisibility = new Dictionary<string, HexTile.EVisibility>();

		// Darken map completely
		List<string> tileKeys = map.GetTileKeys();
		foreach (string key in tileKeys)
		{
			if(map[key].Visibility != HexTile.EVisibility.Hidden)
			{
				tileVisibility.Add(key, HexTile.EVisibility.Darkened);
			}
			else
			{
				tileVisibility.Add(key, HexTile.EVisibility.Hidden);
			}
		}

		// Light up visible tiles
		HexTile current = null;
		int q = 0;
		int r = 0;
		string tileKey = "";
		for (int i = 0; i < areas.Count; i++)
		{
			// Get center tile and light it up
			current = map.GetTileAt(areas[i].Position);
			tileVisibility[HexMap.GetKey(current.Q, current.R)] = HexTile.EVisibility.Lit;

			// Spiral around the center and light all tiles along the way
			for(int spiral = 1; spiral <= areas[i].Radius; spiral++)
			{
				// Get first tile on next ring
				q = current.Q - spiral;
				r = current.R + spiral;
				tileKey = HexMap.GetKey(q, r);

				// Light ring at current distance from center
				for(int dir = 0; dir < 6; dir++)
				{
					for(int neighbor = 0; neighbor < spiral; neighbor++)
					{
						if (tileVisibility.ContainsKey(tileKey))
						{
							tileVisibility[tileKey] = HexTile.EVisibility.Lit;
						}

						CalculateCoords(dir, ref q, ref r);
						tileKey = HexMap.GetKey(q, r);
					}
				}
			}
		}

		// Update actual tile visibilities
		foreach (KeyValuePair<string, HexTile.EVisibility> vis in tileVisibility)
		{
			map[vis.Key].SetVisibility(vis.Value);
		}
	}

	private void CalculateCoords(int _Direction, ref int _Q, ref int _R)
	{
		switch (_Direction)
		{
			case 0:
				{
					_Q++;
				} break;

			case 1:
				{
					_Q++;
					_R--;
				} break;

			case 2:
				{
					_R--;
				} break;

			case 3:
				{
					_Q--;
				} break;

			case 4:
				{
					_Q--;
					_R++;
				} break;

			case 5:
				{
					_R++;
				} break;
		}
	}



	/// <summary>
	/// Prepares a new map visibility, setting a new map and deleting all previous visible areas.
	/// </summary>
	/// <param name="_Map">The new map to use.</param>
	static public void Prepare(HexMap _Map)
	{
		instance = new MapVisibility(_Map);
	}

	/// <summary>
	/// Releases the current map visibility instance.
	/// </summary>
	static public void Release()
	{
		if(instance != null)
		{
			instance.Destroy();
			instance = null;
		}
	}

	/// <summary>
	/// Updates all visible areas, refreshing overall map visibility.
	/// </summary>
	static public void RefreshVisibility()
	{
		instance.UpdateVisibleAreas();
	}

	/// <summary>
	/// Returns the first visible area of the specified target.
	/// </summary>
	/// <param name="_Target">The target associated with the area.</param>
	/// <returns>
	/// The first instance of <see cref="VisibleArea"/> with the specified target;
	/// <c>null</c> if none was found.
	/// </returns>
	static public VisibleArea GetVisibleArea(Transform _Target)
	{
		if (instance == null)
			return null;

		for (int i = 0; i < instance.areas.Count; i++)
		{
			if (instance.areas[i].Target == _Target)
			{
				return instance.areas[i];
			}
		}

		return null;
	}

	/// <summary>
	/// Returns the visible area of the specified position.
	/// </summary>
	/// <param name="_Position">The position to search an area for.</param>
	/// <returns>
	/// The first instance of <see cref="VisibleArea"/> at the specified position without a transform target;
	/// <c>null</c> if none was found.
	/// </returns>
	static public VisibleArea GetVisibleArea(Vector3 _Position)
	{
		if (instance == null)
			return null;

		for (int i = 0; i < instance.areas.Count; i++)
		{
			if (instance.areas[i].Target == null && instance.areas[i].Position == _Position)
			{
				return instance.areas[i];
			}
		}

		return null;
	}

	/// <summary>
	/// Adds a dynamic visible area to the map.
	/// </summary>
	/// <param name="_Target">The target to follow.</param>
	/// <param name="_Radius">The radius of the area.</param>
	static public void AddVisibility(Transform _Target, int _Radius)
	{
		if (instance == null)
			return;

		instance.areas.Add(new VisibleArea(_Target, _Radius));
		MapVisibility.RefreshVisibility();
	}

	/// <summary>
	/// Adds a static visible area to the map.
	/// </summary>
	/// <param name="_Position">The position to observe.</param>
	/// <param name="_Radius">The radius of the area.</param>
	static public void AddVisibility(Vector3 _Position, int _Radius)
	{
		if (instance == null)
			return;

		instance.areas.Add(new VisibleArea(_Position, _Radius));
		MapVisibility.RefreshVisibility();
	}
}