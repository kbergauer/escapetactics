﻿using UnityEngine;
using System.Collections;

public interface IHidable 
{
	void SetVisibility(HexTile.EVisibility _Visibility);
}