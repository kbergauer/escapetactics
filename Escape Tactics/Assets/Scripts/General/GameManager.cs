﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using System.Collections;
using System.Collections.Generic;

public class GameManager : NetworkBehaviour 
{
	[Header("Settings")]
	public float TurnTime = 5.0f;

	private GameManagerUI ui;

	private List<NetworkConnection> connections;
	private List<GameObject> instances;

	private List<int> connectionsWithoutMap;

	private Coroutine turnTimerRoutine = null;
	private ClientPlayer[] players = null;
	private int[] turnSequence = null;
	[SyncVar]
	private int activePlayer = 0;



	public HexMap Map
	{
		get;
		private set;
	}

	public ClientPlayer Active
	{
		get 
		{
			if (turnSequence == null || activePlayer >= turnSequence.Length)
				return null;
			else if (turnSequence[activePlayer] >= players.Length)
				return null;
			else
				return players[turnSequence[activePlayer]]; 
		}
	}



	#region SHARED

	void Awake()
	{
		if(!isServer)
		{
			NetworkManager.singleton.client.Send(MsgType.Ready, new IntegerMessage(NetworkManager.singleton.client.connection.connectionId));
		}
	}

	void Start()
	{
		Map = null;
		ui = FindObjectOfType<GameManagerUI>();
	}

	void OnDestroy()
	{
		if (isServer)
		{
			NetworkServer.UnregisterHandler(CustomMsgTypes.MapReceived);
			NetworkServer.UnregisterHandler(MsgType.Disconnect);
		}

		MapVisibility.Release();
	}

	public void SetMap(HexMap _Map)
	{
		Map = _Map;
		MapVisibility.Prepare(Map);
	}

	public bool IsActive(ClientPlayer _Player)
	{
		return _Player == Active;
	}

	#endregion



	#region SERVER/HOST

	[Server]
	public void Prepare(List<NetworkConnection> _Connections)
	{
		// Prepare lists
		connections = new List<NetworkConnection>();
		instances = new List<GameObject>();

		// Get all connection ids
		connectionsWithoutMap = new List<int>();
		for (int i = 0; i < _Connections.Count; i++)
		{
			if (_Connections[i] != null)
				connectionsWithoutMap.Add(_Connections[i].connectionId);
		}
		//Debug.Log(connectionsWithoutMap.Count + " clients need to receive the map.");

		// Register message handlers
		NetworkServer.RegisterHandler(CustomMsgTypes.MapReceived, OnReceivedMapInform);
		NetworkServer.RegisterHandler(MsgType.Disconnect, OnDisconnection);
	}

	[Server]
	public void AddPlayer(NetworkConnection _Connection, GameObject _Instance)
	{
		connections.Add(_Connection);
		instances.Add(_Instance);
	}

	[Server]
	public void OnDisconnection(NetworkMessage _Message)
	{
		for(int i = 0; i < connections.Count; i++)
		{
			if(!NetworkServer.connections.Contains(connections[i]))
			{
				NetworkServer.Destroy(instances[i]);
				connections.RemoveAt(i);
				instances.RemoveAt(i);
				break;
			}
		}
	}

	[Server]
	public void RunWhenReady()
	{
		// Get all players
		players = FindObjectsOfType<ClientPlayer>();

		// Prepare turn sequence
		turnSequence = new int[players.Length];
		for (int i = 0; i < players.Length; i++)
		{
			turnSequence[i] = i;
		}
		// Shuffle sequence
		int j = 0;
		int value = 0;
		for (int i = 0; i < turnSequence.Length; i++)
		{
			j = i + (int)(Random.Range(0.0f, 1.0f) * (turnSequence.Length - i));
			value = turnSequence[j];
			turnSequence[j] = turnSequence[i];
			turnSequence[i] = value;
		}

		// Inform clients about sequence
		RpcPrepareTurnSequence(turnSequence);

		// Wait for all players to be ready
		StartCoroutine(WaitUntilReady());
	}

	[Server]
	private IEnumerator WaitUntilReady()
	{
		// Wait for all clients to receive the map
		while(connectionsWithoutMap.Count > 0)
		{
			yield return new WaitForFixedUpdate();
		}

		// Rebuild observers for security measures
		NetworkIdentity id = GetComponent<NetworkIdentity>();
		id.RebuildObservers(true);

		// Start the game logic
		Run();
	}

	[Server]
	private void OnReceivedMapInform(NetworkMessage _NetMsg)
	{
		GameSetup.MapReceivedMessage msg = _NetMsg.ReadMessage<GameSetup.MapReceivedMessage>();
		//Debug.Log("Connection " + msg.ConnectionID + " received the map.");

		// Remove id from list
		for (int i = 0; i < connectionsWithoutMap.Count; i++)
		{
			if (connectionsWithoutMap[i] == msg.ConnectionID)
			{
				connectionsWithoutMap.RemoveAt(i);
				break;
			}
		}
	}

	[Server]
	private void Run()
	{
		Debug.Log("Running the game logic!");

		RpcFade(true);
		EndPlayerTurn();
	}

	[Server]
	private IEnumerator RunTurnTimer()
	{
		float startTime = Time.time;
		float total = TurnTime * 60.0f;
		while(Time.time - startTime <= total)
		{
			// TODO: Announce, when there is only 1 minute left
			yield return new WaitForEndOfFrame();
		}

		// Announce that turn time ran out
		RpcAnnounce("Time's up!");

		EndPlayerTurn();
	}

	[Server]
	private void EndPlayerTurn()
	{
		// End current turn
		if (turnTimerRoutine != null)
		{
			StopCoroutine(turnTimerRoutine);
			turnTimerRoutine = null;
		}

		// Advance active player
		activePlayer++;
		if(activePlayer >= turnSequence.Length)
			activePlayer = 0;

		Active.CurrentActions = Active.MaxActions;
		
		// Start next turn
		RpcAnnounce(Active.Name + "'s Turn");
		turnTimerRoutine = StartCoroutine(RunTurnTimer());
	}

	[Server]
	public void PlayerPerformedAction()
	{
		if (Active.CurrentActions <= 0)
			EndPlayerTurn();
	}

	[Server]
	public void Announce(string _Message)
	{
		if(!string.IsNullOrEmpty(_Message))
			RpcAnnounce(_Message);
	}

	#endregion



	#region CLIENT

	[ClientRpc]
	private void RpcPrepareTurnSequence(int[] _Sequence)
	{
		players = FindObjectsOfType<ClientPlayer>();
		turnSequence = _Sequence;
	}

	[ClientRpc]
	private void RpcFade(bool _In)
	{
		if (_In)
			ui.FadeIn(1.0f);
		else
			ui.FadeOut(1.0f);
	}

	[ClientRpc]
	private void RpcAnnounce(string _Message)
	{
		ui.Announce(_Message);
	}

	#endregion
}