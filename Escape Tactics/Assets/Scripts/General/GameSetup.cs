﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using System.Collections.Generic;

public class GameSetup : NetworkBehaviour
{
	#region MESSAGES

	public class MapCreationMessage : MessageBase
	{
		public string[] TileKeys;
		public int PacketIndex;
		public int PacketCount;
	}

	public class MapReceivedMessage : MessageBase
	{
		public int ConnectionID;
	}

	#endregion



	#region SERVER/HOST

	public GameObject PlayerPrefab;

	private List<Transform> spawnPoints;



	[Server]
	public void CreateMap(GameManager _Manager)
	{
		//Debug.Log("Creating map.");

		// Generate map
		MapGenerator gen = GetComponent<MapGenerator>();
		HexMap map = gen.Generate(15);
		_Manager.SetMap(map);

		// Send map to clients
		SendMapCreationMessage(map);

		// Get spawn points for later reference
		GameObject[] points = GameObject.FindGameObjectsWithTag("PlayerSpawn");
		spawnPoints = new List<Transform>();
		for(int i = 0; i < points.Length; i++)
		{
			spawnPoints.Add(points[i].transform);
		}
	}

	[Server]
	private void SendMapCreationMessage(HexMap _Map)
	{
		List<string> keys = _Map.GetTileKeys();
		int index = 0;
		int count = (keys.Count / 100) + 1;
		while(keys.Count > 0)
		{
			index++;

			// Create message
			MapCreationMessage msg = new MapCreationMessage();
			msg.TileKeys = keys.GetRange(0, Mathf.Min(100, keys.Count)).ToArray();

			// Remove the range being sent
			keys.RemoveRange(0, Mathf.Min(100, keys.Count));
			msg.PacketIndex = index;
			msg.PacketCount = count;

			// Send message to all clients
			//Debug.Log("Sending map tile key packet " + msg.PacketIndex + " of " + count + ", " + keys.Count + " remaining..");
			NetworkServer.SendToAll(CustomMsgTypes.MapCreation, msg);
		}
	}

	[Server]
	public void SpawnPlayers(GameManager _Manager, List<NetworkConnection> _Connections, List<short> _IDs, Dictionary<int, string> _Names)
	{
		GameObject obj = null;
		ClientPlayer player = null;
		Vector3 spawn = Vector3.zero;
		int spawnIndex = 0;
		for (int i = 0; i < _Connections.Count; i++)
		{
			//Debug.Log("Spawning player for connection " + _Connections[i].connectionId + " with player id " + _IDs[i]);

			if (spawnPoints.Count > 0)
			{
				spawnIndex = Random.Range(0, spawnPoints.Count);
				spawn = spawnPoints[spawnIndex].position;
				spawnPoints.RemoveAt(spawnIndex);
			}
			else
				spawn = Vector3.zero;

			obj = Instantiate(PlayerPrefab, spawn, Quaternion.identity) as GameObject;
			player = obj.GetComponent<ClientPlayer>();
			if (player != null && _Names.ContainsKey(_Connections[i].connectionId))
				player.Name = _Names[_Connections[i].connectionId];

			NetworkServer.AddPlayerForConnection(_Connections[i], obj, _IDs[i]);
			_Manager.AddPlayer(_Connections[i], obj);
		}
	}

	#endregion



	#region CLIENT

	private List<string> tileKeys = new List<string>();
	private int packetCount = 0;



	[Client]
	public void OnMapCreation(NetworkMessage _NetMsg)
	{
		MapCreationMessage msg = _NetMsg.ReadMessage<MapCreationMessage>();
		//Debug.Log("Received packet " + msg.PacketIndex + " of " + msg.PacketCount + " containing " + msg.TileKeys.Length + " tile keys.");
		tileKeys.AddRange(msg.TileKeys);

		packetCount++;

		if (packetCount == msg.PacketCount)
		{
			//Debug.Log("Received " + packetCount + " packets containing " + tileKeys.Count + " tile keys in total for map creation.");

			SendMapReceivedMessage();

			if (!isServer)
			{
				MapGenerator gen = GetComponent<MapGenerator>();
				HexMap map = gen.Generate(tileKeys);

				GameManager manager = FindObjectOfType<GameManager>();
				manager.SetMap(map);
			}
		}

		// TODO: Request re-sending of map when missing parts.
	}

	[Client]
	public void SendMapReceivedMessage()
	{
		MapReceivedMessage msg = new MapReceivedMessage();
		msg.ConnectionID = NetworkManager.singleton.client.connection.connectionId;

		NetworkManager.singleton.client.Send(CustomMsgTypes.MapReceived, msg);
	}

	#endregion



	#region SHARED

	void Awake()
	{
		if (!isServer)
		{
			//Debug.Log("Registering handler for CustomMsgTypes.MapCreation (" + CustomMsgTypes.MapCreation + ")");
			NetworkManager.singleton.client.RegisterHandler(CustomMsgTypes.MapCreation, OnMapCreation);

			// Send ready message
			NetworkManager.singleton.client.Send(MsgType.Ready, new IntegerMessage(NetworkManager.singleton.client.connection.connectionId));
		}
	}

	void OnDestroy()
	{
		if (!isServer)
		{
			//Debug.Log("Unregistering handler for CustomMsgTypes.MapCreation (" + CustomMsgTypes.MapCreation + ")");
			
			if(NetworkManager.singleton.client != null)
				NetworkManager.singleton.client.UnregisterHandler(CustomMsgTypes.MapCreation);
		}
	}

	#endregion
}