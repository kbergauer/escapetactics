﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using System.Collections;
using System.Collections.Generic;

public class CustomNetworkManager : NetworkManager
{
	#region MESSAGES

	public class PlayerNameMessage : MessageBase
	{
		public int ConnectionID;
		public string Name;

		public PlayerNameMessage()
		{
			ConnectionID = -1;
			Name = "";
		}

		public PlayerNameMessage(int _ID, string _Name)
		{
			ConnectionID = _ID;
			Name = _Name;
		}
	}

	public class MultiPlayerNamesMessage : MessageBase
	{
		public int[] ConnectionIDs;
		public string[] Names;

		public MultiPlayerNamesMessage()
		{
			ConnectionIDs = null;
			Names = null;
		}

		public MultiPlayerNamesMessage(int[] _IDs, string[] _Names)
		{
			ConnectionIDs = _IDs;
			Names = _Names;
		}
	}

	#endregion



	[Header("Interface")]
	public GameObject MenuContainer;
	public LobbyUI LobbyInterface;

	[Header("Match Settings")]
	public string MenuScene = "Menu";
	public string GameScene = "Game";
	public GameObject Setup;
	public GameObject Manager;

	private bool isClient;
	private bool isServer;
	private List<NetworkConnection> allConnections = new List<NetworkConnection>();
	private List<short> allPlayerIDs = new List<short>();
	private Dictionary<int, string> allNames = new Dictionary<int, string>();



	public enum EManagerState
	{
		Lobby,
		Game
	};

	public EManagerState ManagerState
	{
		get;
		private set;
	}



	#region CLIENT

	public void ConnectAsClient()
	{
		isClient = true;
		isServer = false;
		StartClient();
	}

	public override void OnStartClient(NetworkClient client)
	{
		//Debug.Log("OnStartClient");

		base.OnStartClient(client);

		MenuContainer.SetActive(false);
		LobbyInterface.gameObject.SetActive(true);

		LobbyInterface.Title.text = networkAddress + ":" + networkPort;
	}

	public override void OnStopClient()
	{
		base.OnStopClient();

		if (ManagerState == EManagerState.Lobby)
		{
			LobbyInterface.gameObject.SetActive(false);
			MenuContainer.SetActive(true);
		}
		else if (ManagerState == EManagerState.Game)
		{
			Application.LoadLevel("Menu");
		}

		ManagerState = EManagerState.Lobby;
	}

	public override void OnClientConnect(NetworkConnection conn)
	{
		//Debug.Log("OnClientConnect");

		base.OnClientConnect(conn);

		if (ManagerState == EManagerState.Lobby)
		{
			//ClientScene.AddPlayer(conn, 0);

			client.RegisterHandler(CustomMsgTypes.SetPlayerName, OnSetPlayerName);
			client.RegisterHandler(CustomMsgTypes.RemovePlayerName, OnRemovePlayerName);
			client.RegisterHandler(CustomMsgTypes.AddPlayerNames, OnAddPlayerNames);

			//Debug.Log("Sending player name message");
			conn.Send(CustomMsgTypes.SetPlayerName, new PlayerNameMessage(conn.connectionId, PlayerPrefs.GetString("Name")));
		}
	}

	public override void OnClientDisconnect(NetworkConnection conn)
	{
		base.OnClientDisconnect(conn);

		if (client != null)
		{
			client.UnregisterHandler(CustomMsgTypes.SetPlayerName);
			client.UnregisterHandler(CustomMsgTypes.RemovePlayerName);
			client.UnregisterHandler(CustomMsgTypes.AddPlayerNames);
		}
	}

	public override void OnClientSceneChanged(NetworkConnection conn)
	{
		base.OnClientSceneChanged(conn);

		if(networkSceneName == GameScene)
		{
			ManagerState = EManagerState.Game;

			client.UnregisterHandler(CustomMsgTypes.SetPlayerName);
			client.UnregisterHandler(CustomMsgTypes.RemovePlayerName);
			client.UnregisterHandler(CustomMsgTypes.AddPlayerNames);
		}
		else
		{
			ManagerState = EManagerState.Lobby;
		}
	}

	private void OnSetPlayerName(NetworkMessage _NetMsg)
	{
		// Get message
		PlayerNameMessage msg = _NetMsg.ReadMessage<PlayerNameMessage>();

		// Create display
		LobbyInterface.AddPlayerName(msg.ConnectionID, msg.Name);
	}

	private void OnRemovePlayerName(NetworkMessage _NetMsg)
	{
		// Get message
		IntegerMessage msg = _NetMsg.ReadMessage<IntegerMessage>();

		// Remove corresponding display
		LobbyInterface.RemovePlayerName(msg.value);
	}

	private void OnAddPlayerNames(NetworkMessage _NetMsg)
	{
		MultiPlayerNamesMessage msg = _NetMsg.ReadMessage<MultiPlayerNamesMessage>();

		Debug.Log("OnAddPlayerNames with " + msg.Names.Length + " entries.");

		// Add name display for each entry
		for(int i = 0; i < msg.Names.Length; i++)
		{
			LobbyInterface.AddPlayerName(msg.ConnectionIDs[i], msg.Names[i]);
		}
	}

	#endregion



	#region SERVER

	public void ConnectAsHost()
	{
		isClient = true;
		isServer = true;
		StartHost();
	}

	public override void OnStartServer()
	{
		base.OnStartServer();

		ManagerState = EManagerState.Lobby;

		if (LobbyInterface.StartButton != null)
		{
			LobbyInterface.StartButton.SetActive(true);
			LobbyInterface.ReadyToggle.gameObject.SetActive(false);
		}

		NetworkServer.RegisterHandler(CustomMsgTypes.SetPlayerName, ForwardSetPlayerName);
	}

	public override void OnStopServer()
	{
		base.OnStopServer();

		if (ManagerState == EManagerState.Lobby)
		{
			LobbyInterface.gameObject.SetActive(false);
			MenuContainer.SetActive(true);

			NetworkServer.UnregisterHandler(CustomMsgTypes.SetPlayerName);
		}
		else if (ManagerState == EManagerState.Game)
		{
			Application.LoadLevel("Menu");
		}

		allConnections.Clear();
		allPlayerIDs.Clear();
		allNames.Clear();
	}

	public override void OnServerConnect(NetworkConnection conn)
	{
		//Debug.Log("Connected: " + conn.connectionId);

		// Only accept clients in lobby
		if (ManagerState == EManagerState.Game)
		{
			conn.Disconnect();
			return;
		}
		else
		{
			base.OnServerConnect(conn);

			// Instantiate all name displays for client
			MultiPlayerNamesMessage msg = LobbyInterface.GetAllRegisteredInfo();
			//Debug.Log("Sending AddPlayerNames with " + msg.Names.Length + " entries.");
			NetworkServer.SendToClient(conn.connectionId, CustomMsgTypes.AddPlayerNames, msg);
		}
	}

	public override void OnServerDisconnect(NetworkConnection conn)
	{
		if (ManagerState == EManagerState.Lobby)
		{
			NetworkServer.SendToAll(CustomMsgTypes.RemovePlayerName, new IntegerMessage(conn.connectionId));
		}
		else
		{
			base.OnServerDisconnect(conn);
		}
	}

	public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId)
	{
		if (ManagerState == EManagerState.Lobby && NetworkServer.active)
		{
			//Debug.Log("Adding player with connection id " + conn.connectionId + " and player id " + playerControllerId);

			allConnections.Add(conn);
			allPlayerIDs.Add(playerControllerId);
		}
	}

	public override void OnServerRemovePlayer(NetworkConnection conn, PlayerController player)
	{
		if (ManagerState == EManagerState.Game)
		{
			base.OnServerRemovePlayer(conn, player);
		}
	}

	private void ForwardSetPlayerName(NetworkMessage _NetMsg)
	{
		PlayerNameMessage msg = _NetMsg.ReadMessage<PlayerNameMessage>();
		msg.ConnectionID = _NetMsg.conn.connectionId;

		//Debug.Log("Received SetPlayerName message with id " + msg.ConnectionID + " and name " + msg.Name + "; forwarding..");
		NetworkServer.SendToAll(CustomMsgTypes.SetPlayerName, msg);
	}

	public override void OnServerSceneChanged(string sceneName)
	{
		base.OnServerSceneChanged(sceneName);

		if(ManagerState == EManagerState.Game)
		{
			// Unregister lobby handlers
			NetworkServer.UnregisterHandler(CustomMsgTypes.SetPlayerName);

			// Start game setup and run logic
			StartCoroutine(SetupGame());
		}
	}

	public void StartGame()
	{
		allNames = LobbyInterface.GetIDNamePairs();

		ManagerState = EManagerState.Game;
		ServerChangeScene(GameScene);
	}

	private IEnumerator SetupGame()
	{
		// Spawn game manager
		GameObject obj = Instantiate(Manager) as GameObject;
		NetworkServer.Spawn(obj);

		// Prepare game manager on server
		GameManager gm = obj.GetComponent<GameManager>();
		gm.Prepare(allConnections);

		// Set all clients to not ready
		NetworkServer.SetAllClientsNotReady();

		// Spawn game setup object
		obj = Instantiate(Setup) as GameObject;
		NetworkServer.Spawn(obj);

		// Wait for all clients to spawn the game setup and get ready
		List<NetworkConnection> notReady = new List<NetworkConnection>(allConnections);
		while (notReady.Count > 0)
		{
			for (int i = 0; i < notReady.Count; i++)
			{
				if(notReady[i].isReady)
				{
					notReady.RemoveAt(i);
					i--;
				}
			}

			yield return new WaitForFixedUpdate();
		}

		// Create the map and spawn the players
		GameSetup setup = obj.GetComponent<GameSetup>();
		setup.CreateMap(gm);
		setup.SpawnPlayers(gm, allConnections, allPlayerIDs, allNames);

		// Start game logic
		gm.RunWhenReady();
	}

	#endregion



	#region SHARED

	public void Disconnect()
	{
		if (isServer)
		{
			if(isClient)
				StopHost();
			else
				StopServer();
		}
		else
		{
			//ClientScene.RemovePlayer(0);
			StopClient();
		}
	}

	void Start()
	{
		LobbyInterface.gameObject.SetActive(false);
		MenuContainer.SetActive(true);
	}

	void OnLevelWasLoaded(int _Level)
	{
		if(Application.loadedLevelName == MenuScene)
		{
			NetworkManagerUIAccess access = FindObjectOfType<NetworkManagerUIAccess>();
			if(access != null)
			{
				MenuContainer = access.Menu;
				LobbyInterface = access.Lobby;
			}
		}
	}

	#endregion
}