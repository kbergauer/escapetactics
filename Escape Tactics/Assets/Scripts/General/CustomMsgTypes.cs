﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class CustomMsgTypes
{
	// GameSetup
	public static short MapCreation = MsgType.Highest + 1;
	public static short MapReceived = MsgType.Highest + 2;

	// CustomNetworkManager
	public static short SetPlayerName = MsgType.Highest + 3;
	public static short RemovePlayerName = MsgType.Highest + 4;
	public static short AddPlayerNames = MsgType.Highest + 5;
}