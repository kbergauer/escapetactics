﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AnnouncementPlate : MonoBehaviour 
{
	[Header("References")]
	public Text Message;
	public RectTransform LeftEnd;
	public RectTransform RightEnd;

	[Header("Animation")]
	public float Speed = 1.0f;

	private bool showing = false;

	
	
	void OnBecameInvisible()
	{
		Destroy(gameObject);
	}

	public void Show(string _Message, Transform _Parent, float _Duration = 1.0f)
	{
		// Get the rect transform
		RectTransform rect = GetComponent<RectTransform>();

		// Create name plate
		rect.SetParent(_Parent);
		rect.localScale = new Vector3(1.0f, 0.0f, 1.0f);

		// Set text
		Message.text = _Message;

		// Set correct width
		LayoutElement layout = GetComponent<LayoutElement>();
		layout.preferredWidth = LayoutUtility.GetPreferredWidth(Message.transform as RectTransform);

		StartCoroutine(AnimateShow());
		StartCoroutine(HideAfterTime(_Duration));
	}

	private IEnumerator AnimateShow()
	{
		Vector3 scale = transform.localScale;
		while(scale.y < 1.0f)
		{
			scale.y += Time.deltaTime * Speed;
			transform.localScale = scale;
			yield return new WaitForEndOfFrame();
		}

		scale.y = 1.0f;
		transform.localScale = scale;

		showing = true;
	}

	private IEnumerator AnimateHide()
	{
		Vector3 scale = transform.localScale;
		while (scale.y > 0.0f)
		{
			scale.y -= Time.deltaTime * Speed;
			transform.localScale = scale;
			yield return new WaitForEndOfFrame();
		}

		Destroy(gameObject);
	}

	private IEnumerator HideAfterTime(float _WaitingTime)
	{
		while (!showing)
			yield return new WaitForFixedUpdate();

		yield return new WaitForSeconds(_WaitingTime);
		StartCoroutine(AnimateHide());
	}
}