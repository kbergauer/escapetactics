﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameManagerUI : MonoBehaviour 
{
	[Header("Fading")]
	public CanvasGroup FadeGroup = null;

	[Header("Announcements")]
	public AnnouncementPlate AnnouncementPrefab = null;
	public Transform AnnouncementPanel = null;

	private Coroutine fading = null;



	void Start()
	{
		FadeGroup.gameObject.SetActive(true);
	}
	
	public void FadeIn(float _Speed)
	{
		if (FadeGroup != null)
		{
			if (fading != null)
			{
				StopCoroutine(fading);
				fading = null;
			}

			fading = StartCoroutine(Fade(_Speed, 0.0f));
		}
	}

	public void FadeOut(float _Speed)
	{
		if (FadeGroup != null)
		{
			if (fading != null)
			{
				StopCoroutine(fading);
				fading = null;
			}

			fading = StartCoroutine(Fade(_Speed, 1.0f));
		}
	}

	private IEnumerator Fade(float _Speed, float _Limit)
	{
		FadeGroup.transform.parent.SetAsLastSibling();

		if(_Limit <= 0.0f)
		{
			while (FadeGroup.alpha > _Limit)
			{
				FadeGroup.alpha -= _Speed * Time.deltaTime;
				yield return new WaitForEndOfFrame();
			}
		}
		else
		{
			while (FadeGroup.alpha < _Limit)
			{
				FadeGroup.alpha += _Speed * Time.deltaTime;
				yield return new WaitForEndOfFrame();
			}
		}

		FadeGroup.alpha = _Limit;
	}

	public void Announce(string _Message)
	{
		Debug.Log("ANNOUNCEMENT: " + _Message);

		if (AnnouncementPrefab == null)
		{
			return;
		}

		AnnouncementPlate plate = Instantiate(AnnouncementPrefab) as AnnouncementPlate;
		plate.Show(_Message, AnnouncementPanel, 1.0f);
	}
}