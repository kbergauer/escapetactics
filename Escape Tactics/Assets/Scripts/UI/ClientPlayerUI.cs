﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class ClientPlayerUI : ActorUI
{
	public Transform TileHighlightPrefab = null;

	//private ClientPlayer player = null;
	private GameManager manager = null;

	private Transform tileHighlight = null;



	protected override void Start()
	{
		//player = GetComponent<ClientPlayer>();
		manager = FindObjectOfType<GameManager>();
	}

	protected override void Update()
	{
		base.Update();
	}

	public void CursorEnteredTile(HexTileComponent _Tile)
	{
		// Instantiate highlight if none exists
		if (tileHighlight == null)
		{
			tileHighlight = Instantiate(TileHighlightPrefab) as Transform;
		}

		// Activate and position highlight
		tileHighlight.position = _Tile.transform.position;
		tileHighlight.gameObject.SetActive(true);
	}

	public void CursorLeftTile(HexTileComponent _Tile)
	{
		// Disable existing tile highlight
		if (tileHighlight != null)
		{
			tileHighlight.gameObject.SetActive(false);
		}
	}

	public void AddTileHighlight(Vector3 _Position)
	{
		HexTile tile = manager.Map.GetTileAt(_Position);
		if(tile != null)
		{
			CursorEnteredTile(tile.component);
		}
	}

	public void RemoveTileHighlight(Vector3 _Position)
	{
		HexTile tile = manager.Map.GetTileAt(_Position);
		if (tile != null)
		{
			CursorLeftTile(tile.component);
		}
	}
}