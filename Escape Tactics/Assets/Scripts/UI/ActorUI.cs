﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

[RequireComponent(typeof(Collider2D))]
public class ActorUI : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
	[Header("Name Plate")]
	public string Name = "Actor";
	public Transform NamePlatePrefab = null;
	public float NamePlateYOffset = 0.0f;

	protected Transform canvas = null;
	protected RectTransform namePlate = null;
	protected float actorHeight = 0.0f;



	protected virtual void Start()
	{
		SetName(Name);
	}

	protected virtual void Update()
	{
		if(Input.GetKeyDown(KeyCode.Space))
		{
			ShowName();
		}
		if(Input.GetKeyUp(KeyCode.Space))
		{
			HideName();
		}
	}
	
	public virtual void OnPointerEnter(PointerEventData _Data)
	{
		ShowName();

		ClientPlayer.local.UI.AddTileHighlight(transform.position);
	}

	public virtual void OnPointerExit(PointerEventData _Data)
	{
		HideName();

		ClientPlayer.local.UI.RemoveTileHighlight(transform.position);
	}

	public void SetName(string _Name)
	{
		if (string.IsNullOrEmpty(_Name))
			return;

		Name = _Name;
		CreateNamePlate();
	}

	public void CreateNamePlate()
	{
		if (NamePlatePrefab != null)
		{
			// Try getting canvas if it's missing
			if (canvas == null)
			{
				GameObject obj = GameObject.FindGameObjectWithTag("MainCanvas");
				if (obj != null)
					canvas = obj.transform;
			}

			// Create name plate
			namePlate = Instantiate(NamePlatePrefab) as RectTransform;
			namePlate.SetParent(canvas);
			namePlate.position = Vector3.zero;
			namePlate.localScale = Vector3.one;

			// Set text
			Text text = namePlate.GetComponentInChildren<Text>();
			text.text = Name;

			// Set correct width
			Vector2 size = namePlate.sizeDelta;
			size.x = LayoutUtility.GetPreferredWidth(text.transform as RectTransform);
			namePlate.sizeDelta = size;

			namePlate.gameObject.SetActive(false);

			// Get height offset
			Collider2D coll = GetComponent<Collider2D>();
			actorHeight = coll.bounds.size.y * transform.localScale.y;
		}
	}

	public void ShowName()
	{
		if (namePlate == null)
			return;

		Vector3 screenPoint = transform.position;
		screenPoint.y += actorHeight + NamePlateYOffset;
		screenPoint = Camera.main.WorldToScreenPoint(screenPoint);
		namePlate.position = screenPoint;
		namePlate.gameObject.SetActive(true);
	}

	public void HideName()
	{
		if (namePlate == null)
			return;

		namePlate.gameObject.SetActive(false);
	}
}