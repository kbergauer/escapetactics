﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class NetworkManagerUIAccess : MonoBehaviour 
{
	public GameObject JoinPanel = null;
	public GameObject Menu;
	public LobbyUI Lobby = null;

	private CustomNetworkManager netManager;



	void Start()
	{
		netManager = FindObjectOfType<CustomNetworkManager>();
	}

	public void ToggleJoinPanel()
	{
		if(JoinPanel != null)
		{
			if (JoinPanel.activeInHierarchy)
				JoinPanel.SetActive(false);
			else
				JoinPanel.SetActive(true);
		}
	}

	public void Connect()
	{
		netManager.ConnectAsClient();
	}

	public void Host()
	{
		netManager.ConnectAsHost();
	}

	public void Disconnect()
	{
		netManager.Disconnect();
	}

	public void StartGame()
	{
		netManager.StartGame();
	}

	public void SetReady(bool _Ready)
	{

	}
}