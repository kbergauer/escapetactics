﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections.Generic;

public class LobbyUI : MonoBehaviour 
{
	[Header("General")]
	public Text Title;

	[Header("Player List")]
	public Transform NamePanel;
	public GameObject NamePrefab;

	[Header("Settings")]
	public GameObject StartButton;
	public Toggle ReadyToggle;

	private Dictionary<int, Text> nameDisplays;



	void OnEnable()
	{
		nameDisplays = new Dictionary<int, Text>();
	}

	void OnDisable()
	{
		StartButton.SetActive(false);

		foreach (Transform child in NamePanel)
			Destroy(child.gameObject);

		nameDisplays.Clear();
	}

	public GameObject AddPlayerName(int _ConnectionID, string _Name)
	{
		GameObject obj = Instantiate(NamePrefab) as GameObject;
		obj.transform.SetParent(NamePanel);
		obj.transform.localScale = Vector3.one;

		Text display = obj.GetComponent<Text>();
		if(display != null)
			display.text = _Name;

		//Debug.Log("Adding name display for id " + _ConnectionID);
		nameDisplays.Add(_ConnectionID, display);

		return obj;
	}

	public void RemovePlayerName(int _ConnectionID)
	{
		//Debug.Log("Removing name display for id " + _ConnectionID);
		if (nameDisplays.ContainsKey(_ConnectionID))
		{
			Destroy(nameDisplays[_ConnectionID].gameObject);
			nameDisplays.Remove(_ConnectionID);
		}
	}

	public CustomNetworkManager.MultiPlayerNamesMessage GetAllRegisteredInfo()
	{
		List<int> ids = new List<int>();
		List<string> names = new List<string>();

		foreach(KeyValuePair<int, Text> display in nameDisplays)
		{
			ids.Add(display.Key);
			names.Add(display.Value.text);
		}

		return new CustomNetworkManager.MultiPlayerNamesMessage(ids.ToArray(), names.ToArray());
	}

	public Dictionary<int, string> GetIDNamePairs()
	{
		Dictionary<int, string> pairs = new Dictionary<int, string>();

		foreach (KeyValuePair<int, Text> display in nameDisplays)
		{
			pairs.Add(display.Key, display.Value.text);
		}

		return pairs;
	}
}