﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(Text))]
public class ToggleLabel : MonoBehaviour 
{
	public string On = "On";
	public string Off = "Off";

	public Toggle Target;

	private Text self;

	void Start()
	{
		self = GetComponent<Text>();
		if(self == null)
		{
			this.enabled = false;
		}

		if(Target != null)
		{
			Target.onValueChanged.AddListener(TargetValueChanged);
			self.text = Target.isOn ? On : Off;
		}
	}

	public void TargetValueChanged(bool _Flag)
	{
		self.text = _Flag ? On : Off;
	}
}