﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MenuUI : MonoBehaviour 
{
	public InputField NameInput;



	void Start()
	{
		if(NameInput != null)
		{
			string savedName = PlayerPrefs.GetString("Name");
			if (!string.IsNullOrEmpty(savedName))
				NameInput.text = savedName;
		}
	}

	void OnDisable()
	{
		SetName();
	}



	public void SetName()
	{
		if(NameInput != null && !string.IsNullOrEmpty(NameInput.text))
		{
			PlayerPrefs.SetString("Name", NameInput.text);
		}
		else
		{
			PlayerPrefs.SetString("Name", "Nameless");
		}
	}

	public void QuitGame()
	{
		Application.Quit();
	}
}