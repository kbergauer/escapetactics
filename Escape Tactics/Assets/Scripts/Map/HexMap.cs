﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Class holding a grid of hexagonal tiles.
/// </summary>
[System.Serializable]
public class HexMap 
{
	private Dictionary<string, HexTile> grid;
	private bool instantiated = false;

	private Vector2 tileOffset = Vector2.one;



	public HexMap()
	{
		grid = new Dictionary<string, HexTile>();
	}

	public HexMap(Dictionary<string, HexTile> _Grid)
	{
		grid = _Grid;
	}

	public HexTile this[string _Key]
	{
		get
		{
			if (grid.ContainsKey(_Key))
				return grid[_Key];
			else
				return null;
		}

		set
		{
			if (value != null)
				grid[_Key] = value;
			else
				grid.Remove(_Key);
		}
	}

	public HexTile this[int _Q, int _R]
	{
		get
		{
			return this[GetKey(_Q, _R)];
		}

		set
		{
			this[GetKey(_Q, _R)] = value;
		}
	}



	public void SetTileOffset(float _X, float _Y)
	{
		tileOffset = new Vector2(_X, _Y);
	}

	public bool Instantiate()
	{
		if (instantiated)
			return false;

		GameObject parent = new GameObject("Map");
		foreach(KeyValuePair<string, HexTile> tile in grid)
		{
			tile.Value.Instantiate();
			tile.Value.gameObject.name = "Tile (" + tile.Key + ")";
			tile.Value.gameObject.transform.SetParent(parent.transform);
		}

		instantiated = true;
		return true;
	}

	public List<string> GetTileKeys()
	{
		return new List<string>(grid.Keys);
	}

	public List<HexTile> GetTiles()
	{
		return new List<HexTile>(grid.Values);
	}

	public HexTile GetTileAt(Vector3 _Position)
	{
		int q = (int)(_Position.x / tileOffset.x);
		int r = 0;
		if(q != 0)
			r = Mathf.RoundToInt(((_Position.y - (q * tileOffset.y)) / (tileOffset.y * 2.0f)));
		else
			r = Mathf.RoundToInt((_Position.y / (tileOffset.y * 2.0f)));

		if(this[GetKey(q, r)].position == _Position)
			return this[GetKey(q, r)];
		else
		{
			foreach (KeyValuePair<string, HexTile> entry in grid)
			{
				if (entry.Value.position == _Position)
					return entry.Value;
			}
		}

		return null;
	}

	public HexTile GetNeighbor(int _Q, int _R, int _Direction)
	{
		HexTile neighbor = null;

		switch(_Direction)
		{
			case 0:
				{
					neighbor = this[_Q + 1, _R];
				} break;

			case 1:
				{
					neighbor = this[_Q + 1, _R - 1];
				} break;

			case 2:
				{
					neighbor = this[_Q, _R - 1];
				} break;

			case 3:
				{
					neighbor = this[_Q - 1, _R];
				} break;

			case 4:
				{
					neighbor = this[_Q - 1, _R + 1];
				} break;

			case 5:
				{
					neighbor = this[_Q, _R + 1];
				} break;
		}

		return neighbor;
	}

	public HexTile GetNeighbor(HexTile _Tile, int _Direction)
	{
		return GetNeighbor(_Tile.Q, _Tile.R, _Direction);
	}

	public List<HexTile> GetNeighbors(int _Q, int _R)
	{
		List<HexTile> neighbors = new List<HexTile>();
		HexTile tile = null;

		for (int i = 0; i < 6; i++)
		{
			tile = GetNeighbor(_Q, _R, i);

			if (tile != null)
				neighbors.Add(tile);
		}

		return neighbors;
	}

	public List<HexTile> GetNeighbors(HexTile _Tile)
	{
		return GetNeighbors(_Tile.Q, _Tile.R);
	}



	static public string GetKey(int _Q, int _R)
	{
		return string.Format("{0}/{1}", _Q, _R);
	}
}