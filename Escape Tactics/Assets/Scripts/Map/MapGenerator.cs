﻿using UnityEngine;
using System.Collections.Generic;

public class MapGenerator : MonoBehaviour
{
	public GameObject TilePrefab;
	public float TileOffsetX = 1.0f;
	public float TileOffsetY = 1.0f;

	private int radius = 1;
	private HexMap map;



	public HexMap Generate(int _Radius)
	{
		// Abort if size is invalid
		if (_Radius <= 0)
			return null;

		// Prepare map
		radius = _Radius;
		map = new HexMap();
		map.SetTileOffset(TileOffsetX, TileOffsetY);

		// Create full hexagon shape
		CreateHexagon();

		// TODO: Create rooms and hallways

		// Instantiate tiles
		map.Instantiate();

		// Return map for storage
		return map;
	}

	private void CreateHexagon()
	{
		Vector3 position = Vector3.zero;
		for (int q = -radius; q < radius + 1; q++)
		{
			int r = 0;
			if (q < 0)
				r -= (radius + q);
			else
				r -= radius;

			int rMax = radius + 1;
			if (q > 0)
				rMax -= q;

			for (; r < rMax; r++)
			{
				position.z = 0.0f;
				position.x = q * TileOffsetX;
				if (q != 0)
					position.y = r * (TileOffsetY * 2) + (q * TileOffsetY);
				else
					position.y = r * (TileOffsetY * 2);

				map[q, r] = new HexTile(q, r, position, TilePrefab);
			}
		}
	}



	public HexMap Generate(List<string> _TileKeys)
	{
		HexMap map = new HexMap();
		map.SetTileOffset(TileOffsetX, TileOffsetY);

		string[] index = null;
		int q = 0;
		int r = 0;
		Vector3 position = Vector3.zero;

		// Create tiles from keys
		for(int i = 0; i < _TileKeys.Count; i++)
		{
			// Get row and column from key
			index = _TileKeys[i].Split('/');
			q = int.Parse(index[0]);
			r = int.Parse(index[1]);

			// Add tile to map
			position.z = 0.0f;
			position.x = q * TileOffsetX;
			if (q != 0)
				position.y = r * (TileOffsetY * 2) + (q * TileOffsetY);
			else
				position.y = r * (TileOffsetY * 2);

			map[q, r] = new HexTile(q, r, position, TilePrefab);
		}

		// Instantiate all tiles
		map.Instantiate();

		// Return map for reference
		return map;
	}
}