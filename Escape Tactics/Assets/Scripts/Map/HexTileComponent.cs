﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class HexTileComponent : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
	public int Q;
	public int R;

	private ClientPlayer player;

	private SpriteRenderer rend;
	private Collider2D coll;



	public HexTile.EVisibility Visibility
	{
		get;
		private set;
	}



	void Start()
	{
		ClientPlayer[] players = FindObjectsOfType<ClientPlayer>();
		for(int i = 0; i < players.Length; i++)
		{
			if(players[i].hasAuthority)
			{
				player = players[i];
				break;
			}
		}

		rend = GetComponent<SpriteRenderer>();
		coll = GetComponent<Collider2D>();

		Visibility = HexTile.EVisibility.Hidden;
	}

	public void OnPointerEnter(PointerEventData _Data)
	{
		if(player != null)
			player.UI.CursorEnteredTile(this);
	}

	public void OnPointerExit(PointerEventData _Data)
	{
		if (player != null)
			player.UI.CursorLeftTile(this);
	}

	public void OnPointerClick(PointerEventData _Data)
	{
		player.ClickedTile(this);
	}

	public void SetVisibility(HexTile.EVisibility _Visibility)
	{
		// Ignore if visibility is already the specified value
		if(Visibility == _Visibility)
			return;

		Visibility = _Visibility;

		switch(Visibility)
		{
			case HexTile.EVisibility.Hidden:
				{
					rend.enabled = false;
					coll.enabled = false;
				} break;

			case HexTile.EVisibility.Darkened:
				{
					rend.enabled = true;
					coll.enabled = false;
					rend.color = new Color(0.2f, 0.2f, 0.2f);
				} break;

			case HexTile.EVisibility.Lit:
				{
					rend.enabled = true;
					coll.enabled = true;
					rend.color = Color.white;
				} break;
		}

		// Set visibility on all children
		IHidable hideable = null;
		foreach(Transform child in transform)
		{
			hideable = child.gameObject.GetInterface<IHidable>();
			if (hideable != null)
				hideable.SetVisibility(Visibility);
		}
	}
}