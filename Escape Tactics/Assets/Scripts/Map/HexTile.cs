﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

/// <summary>
/// Class representing a hexagonal tile.
/// </summary>
[System.Serializable]
public class HexTile 
{
	public enum EVisibility
	{
		Hidden,
		Darkened,
		Lit
	};



	private int q;
	private int r;
	private GameObject prefab;



	public int Q
	{
		get { return q; }
	}

	public int R
	{
		get { return r; }
	}

	public GameObject gameObject
	{
		get;
		private set;
	}

	public HexTileComponent component
	{
		get;
		private set;
	}

	public Vector3 position
	{
		get;
		private set;
	}

	public EVisibility Visibility
	{
		get { return component.Visibility; }
	}



	public HexTile(int _Q, int _R, Vector3 _Position, GameObject _Prefab)
	{
		q = _Q;
		r = _R;

		position = _Position;
		prefab = _Prefab;
	}

	public void Instantiate()
	{
		gameObject = GameObject.Instantiate(prefab, position, Quaternion.identity) as GameObject;
		component = gameObject.GetComponent<HexTileComponent>();

		component.Q = q;
		component.R = r;
	}

	public bool IsNeighbor(HexTile _Tile)
	{
		int dq = _Tile.q - q;
		int dr = _Tile.r - r;

		return ((dq == 0 || dr == 0) && (Mathf.Abs(dq) == 1 || Mathf.Abs(dr) == 1))
			|| (Mathf.Abs(dq) == 1 && Mathf.Abs(dr) == 1 && dq != dr);
	}

	public void SetVisibility(EVisibility _Visibility)
	{
		if (component != null)
			component.SetVisibility(_Visibility);
	}
}