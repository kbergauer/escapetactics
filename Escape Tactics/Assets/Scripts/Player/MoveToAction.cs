﻿using UnityEngine;
using System.Collections;
using System.Runtime.Serialization;

public class MoveToAction : IPlayerAction 
{
	private int TileQ;
	private int TileR;

	public int Cost
	{
		get { return 1; }
	}



	public MoveToAction(int _Q, int _R)
	{
		TileQ = _Q;
		TileR = _R;
	}

	public bool Execute(GameManager _Manager, ClientPlayer _Player)
	{
		if (!_Player.isServer)
			return false;

		// Get current and new tile from map
		HexTile currentTile = _Manager.Map.GetTileAt(_Player.transform.position);
		HexTile newTile = _Manager.Map[TileQ, TileR];

		// Check movement validity
		if (newTile.IsNeighbor(currentTile))
		{
			// Move player on all clients remotely
			_Player.RpcMoveTo(newTile.position);
			return true;
		}

		return false;
	}
}