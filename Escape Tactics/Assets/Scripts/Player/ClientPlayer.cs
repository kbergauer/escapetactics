﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.EventSystems;
using System.Collections;

public class ClientPlayer : NetworkBehaviour, IHidable
{
	public static ClientPlayer local = null;



	[SyncVar]
	public string Name = "Player";
	[SyncVar]
	public int MaxActions = 3;
	[SyncVar]
	public int CurrentActions = 3;
	public int FieldOfViewRadius = 1;

	private GameManager manager = null;
	private Collider2D coll = null;
	private SpriteRenderer[] rends = null;



	public ClientPlayerUI UI
	{
		get;
		private set;
	}



	#region CLIENT

	[Client]
	void Start()
	{
		gameObject.name = Name;

		UI = GetComponent<ClientPlayerUI>();
		if (UI != null)
			UI.SetName(Name);

		manager = FindObjectOfType<GameManager>();
		coll = GetComponent<Collider2D>();
		rends = GetComponentsInChildren<SpriteRenderer>();

		HexTile parent = manager.Map.GetTileAt(transform.position);
		if (parent != null)
		{
			transform.SetParent(parent.gameObject.transform);
			SetVisibility(parent.Visibility);
		}

		if(!hasAuthority)
		{
			transform.FindChild("Camera").gameObject.SetActive(false);
		}
		else
		{
			local = this;
			coll.enabled = false;

			MapVisibility.AddVisibility(transform, FieldOfViewRadius);
		}
	}

	[Client]
	public void ClickedTile(HexTileComponent _Tile)
	{
		//Debug.Log("Clicked tile at " + _Tile.Q + "/" + _Tile.R + ": " + _Tile.name + "");

		//MoveToAction action = new MoveToAction(_Tile.Q, _Tile.R);
		//CmdMoveTo(action);

		CmdMoveTo(_Tile.Q, _Tile.R);
	}

	[ClientRpc]
	public void RpcMoveTo(Vector3 _Position)
	{
		// Move to new tile
		transform.position = _Position;
		HexTile parent = manager.Map.GetTileAt(_Position);
		if (parent != null)
		{
			transform.SetParent(parent.gameObject.transform);
			SetVisibility(parent.Visibility);
		}

		// Refresh map visibility for local player
		if(hasAuthority)
			MapVisibility.RefreshVisibility();
	}

	[Client]
	public void SetVisibility(HexTile.EVisibility _Visibility)
	{
		// Never allow setting visibility on local player object
		if (hasAuthority)
			return;

		switch(_Visibility)
		{
			case HexTile.EVisibility.Hidden:
			case HexTile.EVisibility.Darkened:
				{
					// Completely hide player
					for (int i = 0; i < rends.Length; i++)
					{
						rends[i].enabled = false;
					}
					coll.enabled = false;
				} break;

			case HexTile.EVisibility.Lit:
				{
					// Show player
					for (int i = 0; i < rends.Length; i++)
					{
						rends[i].enabled = true;
					}
					coll.enabled = true;
				} break;
		}
	}

	#endregion



	#region SERVER

	/*
	[Command]
	public void CmdExecuteAction(IPlayerAction _Action)
	{
		// Only handle active player's requests
		if (!manager.IsActive(this))
		{
			//Debug.Log("Ignoring action execution request by " + Name + " because it is not his turn.");
			return;
		}

		// Check action cost
		if (CurrentActions < _Action.Cost)
		{
			//Debug.Log("Ignoring action execution request by " + Name + " because he does not have enough actions left.");
			return;
		}

		// Try executing action
		if (_Action.Execute(manager, this))
		{
			// Subtract action count and inform game manager of execution
			CurrentActions -= _Action.Cost;
			if (CurrentActions <= 0)
				manager.PlayerPerformedAction();
		}
	}
	*/

	[Command]
	public void CmdMoveTo(int _Q, int _R)
	{
		// Only handle active player's requests
		if (!manager.IsActive(this))
		{
			//Debug.Log("Ignoring action execution request by " + Name + " because it is not his turn.");
			return;
		}

		MoveToAction action = new MoveToAction(_Q, _R);

		// Check action cost
		if (CurrentActions < action.Cost)
		{
			//Debug.Log("Ignoring action execution request by " + Name + " because he does not have enough actions left.");
			return;
		}

		// Try executing action
		if (action.Execute(manager, this))
		{
			// Subtract action count and inform game manager of execution
			CurrentActions -= action.Cost;
			if (CurrentActions <= 0)
				manager.PlayerPerformedAction();
		}
	}

	#endregion
}