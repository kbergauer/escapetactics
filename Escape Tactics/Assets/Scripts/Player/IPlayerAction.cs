﻿using UnityEngine;
using System.Collections;
using System.Runtime.Serialization;

public interface IPlayerAction
{
	int Cost { get; }
	bool Execute(GameManager _Manager, ClientPlayer _Player);
}